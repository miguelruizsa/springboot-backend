CREATE DATABASE pruebas;

CREATE TABLE operador (
                          id_op INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
                          nombre_op VARCHAR(180),
                          PRIMARY KEY (id_op)
);

CREATE TABLE equipo (
                        id_eq INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
                        numser_eq CHAR(20),
                        desc_eq VARCHAR(300),
                        PRIMARY KEY (id_eq)
);

CREATE TABLE incidencia (
                            id_in INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
                            id_op INTEGER UNSIGNED NOT NULL,
                            id_eq INTEGER UNSIGNED NOT NULL,
                            problema_in VARCHAR(1500),
                            fechahora_in DATETIME,
                            estado_in SMALLINT(2),
                            PRIMARY KEY (id_in),
                            FOREIGN KEY (id_op) REFERENCES operador(id_op),
                            FOREIGN KEY (id_eq) REFERENCES equipo(id_eq)
);



INSERT INTO operador (nombre_op)
VALUES ('Operador1'),
       ('Operador2'),
       ('Operador3'),
       ('Operador4'),
       ('Operador5')
;


INSERT INTO equipo (numser_eq, desc_eq)
VALUES ('Equipo1', 'Descripción 1'),
       ('Equipo2', 'Descripción 2'),
       ('Equipo3', 'Descripción 3'),
       ('Equipo4', 'Descripción 4'),
       ('Equipo5', 'Descripción 5')
;


INSERT INTO incidencia (id_op, id_eq, problema_in, fechahora_in, estado_in)
VALUES (3, 2, 'Problema bla, bla, bla...', '2022-11-5', 1);
