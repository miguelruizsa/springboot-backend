package com.examen.backend.controller;

import com.examen.backend.model.Incidencia;
import com.examen.backend.service.IncidenciaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/incidencias")
public class IncidenciaController {

    final IncidenciaService incidenciaService;

    public IncidenciaController(IncidenciaService incidenciaService) {
        super();
        this.incidenciaService = incidenciaService;
    }


    //crear incidencia REST API
    @PostMapping
    public ResponseEntity<Incidencia> crearIncidencia(@RequestBody Incidencia incidencia){

        return new ResponseEntity<>(incidenciaService.saveIncidencia(incidencia), HttpStatus.CREATED);

    }


    //obtiene incidencias REST API
    @GetMapping
    public List<Incidencia> obtenerIncidencias(){
        return incidenciaService.getAllIncidencias();
    }


    //obtiene incidencia por id REST API
    @GetMapping("{id}")
    public ResponseEntity<Incidencia> obtenerIncidenciaPorId(@PathVariable Integer id){

        return new ResponseEntity<>(incidenciaService.getIncidenciaById(id), HttpStatus.OK);

    }


    //actualiza incidencia REST API
    @PutMapping("{id}")
    public ResponseEntity<Incidencia> actualizarIncidencia(@PathVariable Integer id, @RequestBody Incidencia incidencia){

        return new ResponseEntity<>(incidenciaService.updateIncidencia(incidencia, id), HttpStatus.OK);

    }


    //elimina incidencia REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> borrarIncidencia(@PathVariable("id") Integer id){

        // Borrar de la db
        incidenciaService.deleteIncidencia(id);

        return new ResponseEntity<>("Incidencia eliminada correctamente!", HttpStatus.OK);

    }


}
