package com.examen.backend.service.impl;

import com.examen.backend.exception.ResourceNotFoundException;
import com.examen.backend.model.Incidencia;
import com.examen.backend.repository.IncidenciaRepository;
import com.examen.backend.service.IncidenciaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IncidenciaServiceImpl implements IncidenciaService {

    private IncidenciaRepository incidenciaRepository;

    public IncidenciaServiceImpl(IncidenciaRepository incidenciaRepository) {
        super();
        this.incidenciaRepository = incidenciaRepository;
    }

    @Override
    public Incidencia saveIncidencia(Incidencia incidencia) {

        return incidenciaRepository.save(incidencia);

    }

    @Override
    public List<Incidencia> getAllIncidencias() {
        return incidenciaRepository.findAll();
    }

    @Override
    public Incidencia getIncidenciaById(Integer id) {

        Optional <Incidencia> incidencia = incidenciaRepository.findById(id);

        if(incidencia.isPresent()){
            return incidencia.get();
        } else {
            throw new ResourceNotFoundException("Incidencia", "Id", id);
        }

    }

    @Override
    public Incidencia updateIncidencia(Incidencia incidencia, Integer id) {

        //Existe incidencia en db?
        Incidencia existingIncidencia = incidenciaRepository.findById(id).orElseThrow( () -> new ResourceNotFoundException("Incidencia", "Id", id));

        existingIncidencia.setProblema_in(incidencia.getProblema_in());
        existingIncidencia.setEstado_in(incidencia.getEstado_in());

        // guarda incidencia en db
        incidenciaRepository.save(existingIncidencia);

        return existingIncidencia;


    }

    @Override
    public void deleteIncidencia(Integer id) {

        //Existe incidencia en db?
        incidenciaRepository.findById(id).orElseThrow( () -> new ResourceNotFoundException("Incidencia", "Id", id));

        incidenciaRepository.deleteById(id);

    }

}
