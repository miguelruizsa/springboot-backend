package com.examen.backend.service;

import com.examen.backend.model.Incidencia;

import java.util.List;

public interface IncidenciaService {
    Incidencia saveIncidencia(Incidencia incidencia);
    List<Incidencia> getAllIncidencias();
    Incidencia getIncidenciaById(Integer id);
    Incidencia updateIncidencia(Incidencia incidencia, Integer id);
    void deleteIncidencia(Integer id);
}
