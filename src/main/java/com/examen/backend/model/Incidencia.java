package com.examen.backend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="incidencia")
public class Incidencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_in;

    @Column(name = "id_op", nullable = false)
    private Integer id_op;

    @Column(name = "id_eq", nullable = false)
    private Integer id_eq;

    @Column(name = "problema_in")
    private String problema_in;

    @Column(name = "fechahora_in")
    private String fechahora_in;

    @Column(name = "estado_in")
    private int estado_in;

    //@ManyToOne
    //@JoinColumn(name="id_op")
    //Operador operador;

    //@ManyToOne
    //@JoinColumn(name="id_eq")
    //Equipo equipo;

}
