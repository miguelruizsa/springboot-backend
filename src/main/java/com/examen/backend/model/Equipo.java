package com.examen.backend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="equipo")
public class Equipo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_eq;

    @Column(name = "numser_eq")
    private String numser_eq;

    @Column(name = "desc_eq")
    private String desc_eq;

}
